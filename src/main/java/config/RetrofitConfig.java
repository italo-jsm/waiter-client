package config;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import service.LoginService;
import service.ProductService;

public class RetrofitConfig {
    private Retrofit retrofit;

    public RetrofitConfig(){
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public LoginService getLoginService(){
        return this.retrofit.create(LoginService.class);
    }

    public ProductService getProductService(){
        return this.retrofit.create(ProductService.class);
    }
}
