package service;

import model.Credentials;
import model.Product;
import model.Token;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginService {
    @POST("login")
    Call<Token> login(@Body Credentials credentials);
}
