package service;

import model.Credentials;
import model.Product;
import model.Token;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ProductService {
    @POST("products")
    Call<Product> saveProduct(@Body Product product, @Header("Authorization")String auth);
}
