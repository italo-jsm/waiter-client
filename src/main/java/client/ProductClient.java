package client;

import config.RetrofitConfig;
import model.Credentials;
import model.Product;
import model.Token;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.ProductService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.util.*;

public class ProductClient implements Callback<Token>{
    public void sendCsvProducts(){
        RetrofitConfig retrofit = new RetrofitConfig();
        Credentials credentials = new Credentials();
        credentials.setUsername("italo-jsm");
        credentials.setPassword("password");
        retrofit.getLoginService().login(credentials).enqueue(this);
    }

    private void sendProducts(String auth){

        ProductService productService = new RetrofitConfig().getProductService();
        parseProducts().forEach(product -> {
            LocalDateTime init = LocalDateTime.now();
            productService.saveProduct(product, auth).enqueue(new Callback<Product>() {
                @Override
                public void onResponse(Call<Product> call, Response<Product> response) {

                }

                @Override
                public void onFailure(Call<Product> call, Throwable throwable) {
                    System.out.println();
                }
            });
        });
    }

    @Override
    public void onResponse(Call<Token> call, Response<Token> response) {
        Optional<Token> tokenOptional = Optional.ofNullable(response.body());
        tokenOptional.ifPresent(token -> sendProducts("Bearer " + token.token));
    }

    @Override
    public void onFailure(Call<Token> call, Throwable throwable) {

    }

    private List<Product> parseProducts(){
        List<Product> products = new ArrayList<>();
        try {
            File f = new File("seed-products.csv");
            Scanner sc = new Scanner(new FileInputStream(f));
            sc.nextLine();
            while (sc.hasNextLine()){
                String line = sc.nextLine();
                String[] split = line.split(",");
                Product p = new Product();
                p.setCode(split[0]);
                p.setDescription(split[1]);
                p.setProductionCost(Double.valueOf(split[2]));
                p.setSaleCost(Double.valueOf(split[3]));
                products.add(p);
            }
        } catch (FileNotFoundException e) {
            return Collections.emptyList();
        }
        return products;
    }
}
