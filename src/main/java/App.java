import client.ProductClient;
import config.RetrofitConfig;
import model.Credentials;
import model.Token;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class App {
    public static void main(String[] args) {
        ProductClient productClient = new ProductClient();
        productClient.sendCsvProducts();
    }

}
